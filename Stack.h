// Template stack data structure
#pragma once
#include <iostream>
#include <initializer_list>

// Stack list node
template<class T>
struct StackNode
{
	StackNode() : value(T{}), next(nullptr) {}
	StackNode(const T& _value) : value(_value), next(nullptr) {}

	T value;
	StackNode<T>* next = nullptr;
};

// Stack list
template<class U>
class Stack
{
public:
	Stack<U>() : head(nullptr) {}
	Stack<U>(const std::initializer_list<U>& list);

	void Display() const;
	size_t Size() const;
	bool Empty() const { return head ? false : true; }
	U Head() const { return head ? head->value : U{}; }
	U Front() const;

	U Pop();
	void Remove(const U& _value);
	void PushBack(const U& _value);

	void Reverse();

private:
	StackNode<U>* GetBackIter() const;
	StackNode<U>* head;
};

// Initializer list constructor
template<typename U>
Stack<U>::Stack(const std::initializer_list<U>& list) : Stack()
{
	for (const auto& element : list)
	{
		StackNode<U>* newNode = new StackNode<U>;
		newNode->value = element;
		newNode->next = head;
		head = newNode;
	}
}

// Find pointer to front node
template<typename U>
StackNode<U>* Stack<U>::GetBackIter() const
{
	if (!head)
		return nullptr;
	StackNode<U>* current = head;
	while (current->next)
		current = current->next;
	return current;
}

// Reverse stack list
template<typename U>
void Stack<U>::Reverse()
{
	size_t halfSize = Size();
	if (halfSize < 2)
		return;
	halfSize /= 2;

	StackNode<U>* backIter = GetBackIter();
	StackNode<U>* headIter = head;
	StackNode<U>* currentIter;

	for (int i = 0; i < halfSize; ++i)
	{
		U tempValue = headIter->value;
		headIter->value = backIter->value;
		backIter->value = tempValue;
		currentIter = headIter;
		while (currentIter->next != backIter)
			currentIter = currentIter->next;
		backIter = currentIter;
		headIter = headIter->next;
	}
}

// Return size of stack list
template<typename U>
size_t Stack<U>::Size() const
{
	size_t resultSize = 0;
	for (StackNode<U>* current = head; current; current = current->next)
		++resultSize;
	return resultSize;
}

// Return front element of stack
template<typename U>
U Stack<U>::Front() const
{
	StackNode<U>* backIter = GetBackIter();
	return backIter ? backIter->value : U{};
}

// Display stack separated by a space
template<typename U>
void Stack<U>::Display() const
{
	StackNode<U>* pStack = head;
	while (pStack)
	{
		std::cout << pStack->value << ' ';
		pStack = pStack->next;
	}
}

// Extract and return head element of stack
template<typename U>
U Stack<U>::Pop()
{
	if (!head)
		return U{};

	U resultVal = head->value;
	StackNode<U>* current = head;
	head = head->next;
	delete current;
	return resultVal;
}

// Add element to stack
template<typename U>
void Stack<U>::PushBack(const U& _value)
{
	StackNode<U>* newNode = new StackNode<U>(_value);
	newNode->next = head;
	head = newNode;
}

// Remove element by value
template<typename U>
void Stack<U>::Remove(const U& _value)
{
	if (!head)
		return;

	StackNode<U>* current = head;
	while (current)
	{
		if (current->value == _value && current == head)
		{
			current = current->next;
			delete head;
			head = current;
		}
		else if (current->value == _value)
		{
			StackNode<U>* prevNode = head;
			while (prevNode->next != current)
				prevNode = prevNode->next;
			prevNode->next = current->next;
			delete current;
			current = prevNode->next;
		}
		if (!current)
			break;
		current = current->next;
	}
}
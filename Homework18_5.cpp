#include <iostream>
#include <string>
#include "Stack.h"

int main()
{
	// �������� ����� string ����� ������ �������������. ���������� Stack<U>(const std::initializer_list<U>& list)
	Stack<std::string> strStack = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

	// ����� ����������� �����: Display().
	std::cout << "String stack: ";
	strStack.Display();
	std::cout << '\n';

	// ����� ������� �����: Size()
	std::cout << "Stack size: " << strStack.Size() << '\n';

	// ����� ���������� ��������� �����: Head()
	std::cout << "Head element: " << strStack.Head() << '\n';

	// ����� ������� ��������� �����: Front()	
	std::cout << "Front element: " << strStack.Front() << '\n';

	// �������� �� ������� ���������� � �����: Empty()
	std::cout << "Stack is empty: " << strStack.Empty() << '\n';
	std::cout << '\n';

	// ������� � ������� ��������� ��������� ����������: Pop()
	std::cout << "Pop head: " << strStack.Pop() << '\n';
	std::cout << "Pop head: " << strStack.Pop() << '\n';
	std::cout << "Pop head: " << strStack.Pop() << '\n';
	std::cout << "Stack after pop: ";
	strStack.Display();
	std::cout << "\n\n";

	// �������� ��������� ���������� � ����: PushBack(const U& _value)
	strStack.PushBack("eleven");
	strStack.PushBack("ten");
	strStack.PushBack("eleven");
	std::cout << "Stack after push: ";
	strStack.Display();
	std::cout << "\n\n";

	// ������� ��������� �� ����� �� ��������: Remove(const U& _value)
	strStack.Remove("eleven");
	strStack.Remove("ten");
	std::cout << "Stack after remove: ";
	strStack.Display();
	std::cout << "\n\n";

	// ������������� ����: Reverse(const U& _value)
	strStack.Reverse();
	std::cout << "Stack after reverse: ";
	strStack.Display();
	std::cout << "\n\n";

	// �������� ����� double ����� ������ �������������. ���������� Stack<U>(const std::initializer_list<U>& list)
	Stack<double> dblStack = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 };
	
	// ����� ����������� �����: Display().
	std::cout << "Double Stack: ";
	dblStack.Display();
	std::cout << "\n\n";
	
	// ������������� ����: Reverse(const U& _value)
	dblStack.Reverse();
	std::cout << "Double stack after reverse: ";
	dblStack.Display();
	std::cout << '\n';
	
	return 0;
}